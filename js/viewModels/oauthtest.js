/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * oauthtest module
 */

/**
define([['ojs/ojcore', 
        'knockout', 
        'jquery', 
        'ojs/ojmodel', 
        'ojs/ojtable', 
        'ojs/ojcollectiontabledatasource'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
 
    
    
   /** 
    function oauthtestContentViewModel() {
        var self = this;
        self.firstName = ko.observable("Planet");
        self.lastName = ko.observable("Earth");
        self.fullName = ko.pureComputed(function () {
            return this.firstName() + " " + this.lastName();
        }, this);
    }**/
    
    
    
define(['ojs/ojcore', 
        'knockout', 
        'jquery', 
        'ojs/ojmodel', 
        'ojs/ojtable', 
        'ojs/ojcollectiontabledatasource'],
    function (oj, ko, $) {
        //Create a new provider
        var OAuth = require('@zalando/oauth2-client-js');
        var dbrota = new OAuth.Provider({
        id: 'dbrota',   // required
        authorization_url: 'https://oraweb.cern.ch/ords/devdb11/db-rota/auth' // required
        });
        
        
        // Create a new request
        var request = new OAuth.Request({
            client_id: 'my_client_id',  // required
            redirect_uri: 'https://oraweb.cern.ch/ords/devdb11/db-rota/users/redirect'
        });

        // Give it to the provider
        var uri = dbrota.requestToken(request);

        // Later we need to check if the response was expected
        // so save the request
        dbrota.remember(request);

        // Do the redirect
        window.location.href = uri;
        
        var response = dbrota.parse(window.location.hash);
        
        var uri = dbrota.refreshToken();
        yourHttpLibrary
            .get(uri)
            .then(function(response) {
                dbrota.handleRefresh(response.body);

            });
            
        dbrota.getAccessToken();    
        
        dbrota.getRefreshToken();
        
        return new oauthtestContentViewModel();
    }
);
    

