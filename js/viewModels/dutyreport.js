/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * test2 module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodel'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function test2ContentViewModel() {
        var self = this;
               self.serviceURL = 'http://RESTServerIP:Port/stable/rest/Departments';

           parseDept = function(response) {
               if (response['Departments']) {
                   var innerResponse = response['Departments'][0];
                   if (innerResponse.links.Employees == undefined) {
                       var empHref = '';
                   } else {
                       empHref = innerResponse.links.Employees.href;
                   }
                   return {DepartmentId: innerResponse['DepartmentId'],
                           DepartmentName: innerResponse['DepartmentName'],
                           links: {Employees: {rel: 'child', href: empHref}}};
               }
               return {DepartmentId: response['DepartmentId'],
                       DepartmentName: response['DepartmentName'],
                       LocationId:response['LocationId'],
                       ManagerId:response['LocationId'],
                       links: {Employees: {rel: 'child', href: response['links']['Employees'].href}}};
           };

           // Think of this as a single database record or a single table row.
               var Department = oj.Model.extend({
                   urlRoot: self.serviceURL,
                   parse: parseDept,
                   idAttribute: 'DepartmentId'
               });
    
               var myDept = new Department();
           };

           return {'deptVM': viewModel};
       };
}}
