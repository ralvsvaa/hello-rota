/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * domaindefinition module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojgauge','ojs/ojtable'], 
function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function domaindefinitionContentViewModel() {
        var self = this;
        
        
        self.data = ko.observableArray();
        $.getJSON("https://oraweb.cern.ch/ords/devdb11/db-rota/users/").
                then(function (users) {
                    $.each(users, function () {
                        self.data.push({
                            userid: this.userid,
                            name: this.name,
                            surename: this.surename
                        });
                    });
                });
        self.dataSource = new oj.ArrayTableDataSource(
                self.data, 
                {idAttribute: 'userid'}
        );
        
        
        
        
        self.value10 = ko.observable(80); //Gauge
        
        self.firstName = ko.observable("Planet");
        self.lastName = ko.observable("Earth");
        self.fullName = ko.pureComputed(function () {
            return this.firstName() + " " + this.lastName();
        }, this);
    }
    
    return domaindefinitionContentViewModel;
});
