/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * jsotest module
 */
define(['ojs/ojcore', 'knockout', 'jso'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function jsotestContentViewModel() {
        
        var JSO = require("libs/jso/jso");
        var self = this;
        self.firstName = ko.observable("Planet");
        self.lastName = ko.observable("Earth");
        self.fullName = ko.pureComputed(function () {
            return this.firstName() + " " + this.lastName();
        }, this);
    }
    
    return jsotestContentViewModel;
    
    
    define(function(require, exports, module) {

		var JSO = require("libs/jso/jso");
                
                
                var jso = new JSO({
		providerID: "google",
		client_id: "541950296471.apps.googleusercontent.com",
		redirect_uri: "http://bridge.uninett.no/jso/index.html",
		authorization: "https://accounts.google.com/o/oauth2/auth",
		scopes: { request: ["https://www.googleapis.com/auth/userinfo.profile"]}
                });
                
                jso.callback();

		
	});
    
    
});
